# **BEM VINDO AO JOGO DA COBRINHA**

<LINKGIT>

##**Descrição do projeto**
Esse jogo e uma adptação do jogo da cobrinhas, apresentando uma variedade de cobras: A CobraComum, SnakeStar que recebe o dobro de pontos ao comer uma fruta, e Kitty snake que consegue atravessar obstaculos.
E 4 frutas diferentes, Simple fruit a tradicional, Big fruit que acrescenta 2 pontos e 1 pixel no da tamanho na snake, BombFruit que mata a snake e DrecreaseFruit que reduz o tamanho da snake em 1 e não soma ponto


##**Como compilar**
-Necessario ter o JAVA e o JDK instalado no seu Linux/Mac/Windows
-Tenha a IDE do Eclipse instalada, abra o projeto pelo Eclipse e o execute nesse ambiente.

##**Como jogar e como funciona o jogo**
-Ao inicializar o jogo aparecerá um Menu Dando as seguintes opções: 1 - Selecionar a SnakeComum 2 - Sair do jogo
-Para movimentar a snake basta utilizar as setas, e os comandos os respectivos das setas irão ocorrer.
-A pontuação ficará em baixo no canto esquerdo, a partir do momento que o jogador acumular muitos pontos a velocidade do jogo aumenta, consequentemente sua dificuldade.
-Ao colidir em algo a snake morre, e é mostrado na tela a pontuação do jogador, e acima disso uma mensagem de teclar <ENTER> para reinicializar o jogo.
-O arquivo jar executa o jogo diretamente caso tenha os requisitos pedido acima.

Obs: Para visaulizar a UML deve entrar no Eclipse e selecionar a mesma.