import java.awt.Color;
import java.awt.Graphics;

//Heran�a
public class BigFruit extends FrutasMae {
	
	protected static int PIXELSIZE = 35; //TAMANHO
	
	@Override
	public void desenharFood(Graphics g2) {  
        g2.setColor(Color.green);
        g2.fillRect(getFoodX(), getFoodY(), PIXELSIZE, PIXELSIZE); // food
	}
	@Override
	public void pegarPontos() {
		setPontos(getPontos()+2);
	}
}
