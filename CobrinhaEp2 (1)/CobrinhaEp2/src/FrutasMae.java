import java.awt.Color;
import java.awt.Graphics;

public class FrutasMae {
	private CobraMain snake = new CobraMain();
	private int foodX;
	private int foodY; 
	private int pontos = 0;
	protected static int PIXELSIZE = 25; //TAMANHO
	

	
	protected int RANDOMPOSITION = 40;

	public void createFood() {

	    // Setando o X e Y numa posi�ao aleatoria do "mapa" de jogo

	    int location = (int) (Math.random() * RANDOMPOSITION);
	    foodX = ((location * Board.getDotSize()));

	    location = (int) (Math.random() * RANDOMPOSITION);
	    foodY = ((location * Board.getDotSize()));

	    if ((foodX == snake.getSnakeX(0)) && (foodY == snake.getSnakeY(0))) {
	        createFood();
	    }
	    
	}

	public int getFoodX() {

	    return foodX;
	}

	public int getFoodY() {
	    return foodY;
	}
	
	public void mostrarPontos() {
		 if ((foodX == snake.getSnakeX(0)) && (foodY == snake.getSnakeY(0))) {
		        setPontos(getPontos() + 1);
		    }
	}

	public int getPontos() {
		return pontos;
	}

	public void setPontos(int pontos) {
		this.pontos = pontos;
	}

	public void desenharFood(Graphics g2) {  
        g2.setColor(Color.blue); //mudando
        g2.fillRect(getFoodX(), getFoodY(), PIXELSIZE, PIXELSIZE); // food
	}
	
	public void pegarPontos() {
		setPontos(getPontos()+1);
	}
	
}
