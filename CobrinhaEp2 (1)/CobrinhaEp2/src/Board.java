import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JPanel;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class Board extends JPanel implements ActionListener, Runnable {
// Variaveis para o loop game
private Thread thread;
private boolean inGame = true;
private Timer timer;

// Matendo um tamnho padr�o para a janela
private final static int BOARDWIDTH = 1000;
private final static int BOARDHEIGHT = 980;


// Tamanho da snake e fruta e dos pontos
private final static int PIXELSIZE = 25;
private final static int TODOS_PONTOS = 1700;

//Defini��o do plano cartesiano (x,y) do jogo
private int[] x = new int[TODOS_PONTOS];
private int[] y = new int[TODOS_PONTOS];

// Pontos da cobrinha
private int pontos = 0;
// Contar pontua��o
private int PONTUA��O = 0;

// Mensagem da pontua��o
String SCORE = "PONTUA��O: " + PONTUA��O;
// Fonte para escrever a pontua��o, estilo da fonte
Font SCORE_FONT = new Font("Consolas", Font.BOLD, 30);
// Tamanho total da escrita na tela
FontMetrics SCORE_METRICA = this.getFontMetrics(SCORE_FONT);


private final static int TOTALPIXELS = (BOARDWIDTH * BOARDHEIGHT)
        / (PIXELSIZE * PIXELSIZE);

private  int speed = 100;

// Agregando as classes CobraMain e FrutasMain para podermos usar os metodos das mesmas
private CobraMain snake = new CobraMain();
private FrutasMae food = new FrutasMae();

public Board() {

    addKeyListener(new Keys());
    setBackground(Color.BLACK);
    setFocusable(true);
    // Define o tamanho da tela
    setPreferredSize(new Dimension(BOARDWIDTH, BOARDHEIGHT));

    initializeGame();
}

// Pintar os compontes na tela
@Override
protected void paintComponent(Graphics g) {
    super.paintComponent(g);

    draw(g);
}
// M�todo para desenhar a pontua��o na tela
public void desenharPontuacao (Graphics g)
{
    // Define a frase para escrever
    SCORE = "PONTUA��O: " + PONTUA��O;
    // Define o tamanho da fonte
    SCORE_METRICA = this.getFontMetrics(SCORE_FONT);

    // Define a cor da fonte
    g.setColor(Color.white);
    // Seta a fonte para o gr�fico
    g.setFont(SCORE_FONT);
    // Desenha a fonte na tela
    g.drawString(SCORE, (BOARDWIDTH - SCORE_METRICA.stringWidth(SCORE)) - 10, BOARDHEIGHT - 10);
}

// Desenhando a snake e food 
void draw(Graphics g) {
    // So desenha a snake se ela estiver viva
    if (inGame == true) {
        g.setColor(Color.green);
        g.fillRect(food.getFoodX(), food.getFoodY(), PIXELSIZE, PIXELSIZE); // food

        // Desenhando a snake
        for (int i = 0; i < snake.getJoints(); i++) {
            // Cabe�a da snake
            if (i == 0) {
                g.setColor(Color.BLUE);
                g.fillRect(snake.getSnakeX(i), snake.getSnakeY(i),
                        PIXELSIZE, PIXELSIZE);
                // Corpo da snake
            } else {
                g.setColor(Color.YELLOW);
            	g.fillRect(snake.getSnakeX(i), snake.getSnakeY(i),
                        PIXELSIZE, PIXELSIZE);
            }
        }
        // Desenha a pontua��o na tela
        desenharPontuacao(g);
        
        // Sync our graphics together
        Toolkit.getDefaultToolkit().sync();
    } else {
        // Se n�o esta viva, termina o jogo
        endGame(g);
    }
}

void initializeGame() {
    snake.setJoints(3); // set our snake's initial size
    pontos = 0;
    // Cria o corpo da snake
    for (int i = 0; i < snake.getJoints(); i++) {
        snake.setSnakeX(BOARDWIDTH / 2);
        snake.setSnakeY(BOARDHEIGHT / 2);
    }
    // fazendo que ela comece a se movintar indo para a direita
    snake.setMovingRight(true);

    // Cria a primeira comida
    food.createFood();

    // Setando o timer para gravar a velocidade do jogo
    timer = new Timer(speed, this);
    timer.start();
    //Aumentar a dificuldade, not working...
   
    
}

void checkFoodCollisions() {

    if ((proximity(snake.getSnakeX(0), food.getFoodX(), 20))
            && (proximity(snake.getSnakeY(0), food.getFoodY(), 20))) {

        System.out.println("intersection");
        // incrementando o tamanho da snake
        snake.setJoints(snake.getJoints() + 1);
        // Gerando nova comida e atualizando pontos
        food.createFood();
        PONTUA��O++;
        
        if(PONTUA��O >= 20) {
        	speed-= 130;
        }
    }
}

// Used to check collisions with snake's self and board edges
void checkCollisions() {

    // Se a snake colidir com os proprios peda�os
    for (int i = snake.getJoints(); i > 0; i--) {

        // Colis�o com o proprio corpo so eh possivel se tiver um tamanho minimo
        if ((i > 5)
                && (snake.getSnakeX(0) == snake.getSnakeX(i) && (snake
                        .getSnakeY(0) == snake.getSnakeY(i)))) {
            inGame = false; // ent o jogo eh encerrado
        }
    }

    // Checando intesc��o da snake com as bordas do jogo
    if (snake.getSnakeY(0) >= BOARDHEIGHT) {
        inGame = false;
    }

    if (snake.getSnakeY(0) < 0) {
        inGame = false;
    }

    if (snake.getSnakeX(0) >= BOARDWIDTH) {
        inGame = false;
    }

    if (snake.getSnakeX(0) < 0) {
        inGame = false;
    }

    if (!inGame) {
        timer.stop();
    }
}

void endGame(Graphics g) {


	// Define a frase para escrever
    String msg = "GAME OVER! Sua pontua��o: " + PONTUA��O;
    // Define o estilo da fonte
    Font pequena = new Font("Consolas", Font.BOLD, 30);
    // Define o tamanho da fonte
    FontMetrics metrica = this.getFontMetrics(pequena);
    
    // Define a cor da fonte
    g.setColor(Color.white);
    // Seta a fonte para o gr�fico
    g.setFont(pequena);
    // Desenha a fonte na tela
    g.drawString(msg, (BOARDWIDTH - metrica.stringWidth(msg)) / 2, BOARDHEIGHT / 2);
    
    String reset = "Tecle <ENTER> para reincializar o jogo";
    Font res = new Font("Consolas", Font.BOLD, 30);
    FontMetrics novaMetrica = this.getFontMetrics(res);
    g.setColor(Color.DARK_GRAY);
    g.setFont(res);
    g.setFont(res);g.drawString(reset, (BOARDWIDTH - novaMetrica.stringWidth(msg)) / 3, BOARDHEIGHT / 3);
    

}

//Run constantly as long as we're in game.

@Override
public void actionPerformed(ActionEvent e) {
    if (inGame == true) {

        checkFoodCollisions();
        checkCollisions();
        snake.move();

        System.out.println(snake.getSnakeX(0) + " " + snake.getSnakeY(0)
                + " " + food.getFoodX() + ", " + food.getFoodY());
    }
    repaint();
}

private class Keys extends KeyAdapter {

    @Override
    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();
      
        if ((key == KeyEvent.VK_LEFT) && (!snake.isMovingRight())) {
            snake.setMovingLeft(true);
            snake.setMovingUp(false);
            snake.setMovingDown(false);
        }

        if ((key == KeyEvent.VK_RIGHT) && (!snake.isMovingLeft())) {
            snake.setMovingRight(true);
            snake.setMovingUp(false);
            snake.setMovingDown(false);
        }

        if ((key == KeyEvent.VK_UP) && (!snake.isMovingDown())) {
            snake.setMovingUp(true);
            snake.setMovingRight(false);
            snake.setMovingLeft(false);
        }

        if ((key == KeyEvent.VK_DOWN) && (!snake.isMovingUp())) {
            snake.setMovingDown(true);
            snake.setMovingRight(false);
            snake.setMovingLeft(false);
        }

        if ((key == KeyEvent.VK_ENTER) && (inGame == false)) {

            inGame = true;
            snake.setMovingDown(false);
            snake.setMovingRight(false);
            snake.setMovingLeft(false);
            snake.setMovingUp(false);
            PONTUA��O = 0;

            initializeGame();
        }
    }
}

private boolean proximity(int a, int b, int closeness) {
    return Math.abs((long) a - b) <= closeness;
}

public static int getAllDots() {
    return TOTALPIXELS;
}

public static int getDotSize() {
    return PIXELSIZE;
}

@Override
public void run() {
	// TODO Auto-generated method stub
	
}


}