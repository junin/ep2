import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

@SuppressWarnings("serial")
public class Game extends JFrame {

Game() {
    add(new Board());
    setResizable(false);
    pack();

    setTitle("Snake");
    setLocationRelativeTo(null);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
}

	public static void main(String[] args) {


		Integer opc;
	    
	    opc=Integer.parseInt(JOptionPane.showInputDialog("APERTE 1 PARA JOGAR COM SNAKE CLASSICA\nAPERTE 2 PARA SAIR "));
	    
	    switch(opc){
	        case 1:
	        	// THREAD para que o GUI consiga processsar tudo sozinho
	            EventQueue.invokeLater(new Runnable() {
	                @Override
	                public void run() {
	                    JFrame frame = new Game();
	                    frame.setVisible(true);
	                }
	            });
	            break;
	        case 2:
	    }
	}	
}